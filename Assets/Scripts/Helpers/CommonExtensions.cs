﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace org.yastudio
{
	public static class CommonExtensions {
		
		public static GameObject InstantiatePrefab (this GameObject parent, string name, string elementName = @"")
		{
			return parent.InstantiatePrefab(name, Vector3.zero);
		}
		
		public static GameObject InstantiatePrefab (this GameObject parent, string name, Vector3 position, string elementName = @"")
		{
			var res = Resources.Load<GameObject>(name);
			var go = Object.Instantiate(res);
			go.transform.SetParent(parent.transform, false);
			go.transform.localScale = Vector3.one;
			go.transform.localPosition = position;
			
			#if UNITY_EDITOR
			// Game objects name doesnt matter outside the editor
			go.name = string.IsNullOrEmpty(elementName) ? go.name.Replace(@"(Clone)", String.Empty) : elementName;
			#endif

			return go;
		}

		public static void DestroyAllChildren(this GameObject parent)
		{
			for (var i = parent.transform.childCount - 1; i >= 0; i--)
				Object.DestroyImmediate(parent.transform.GetChild(i).gameObject);
		}

		public static T GetOrCreateComponent<T>(this GameObject go) where T: MonoBehaviour
		{
			var o = go.GetComponent<T>();

			return o ?? go.AddComponent<T>();
		}

		public static void TryToRemoveComponent<T>(this GameObject go) where T: MonoBehaviour
		{
			var o = go.GetComponent<T>();

			if (o != null)
				Object.DestroyImmediate(o);
		}

		public static void Dispatch ( this Action e )
		{
			if ( e != null )
				e();
		}

		public static void Dispatch<T>(this Action<T> e, T item)
		{
			if ( e != null )
				e( item );
		}

		public static void Dispatch<T, T1>(this Action<T, T1> e, T item, T1 item1)
		{
			if (e != null)
				e(item, item1);
		}

		public static void Dispatch<T, T1, T2>(this Action<T, T1, T2> e, T item, T1 item1, T2 item2)
		{
			if (e != null)
				e(item, item1, item2);
		}
	}
}

