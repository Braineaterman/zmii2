﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace org.yastudio.Zmii
{
	public enum ItemType { FreePart, Wall, Snake, Bonus }
	
	public class BaseGameItem
	{
		protected ItemType _type = ItemType.FreePart;
		protected Vector2Int _position = Vector2Int.zero;
		protected Vector2Int _size = Vector2Int.one;
		
		public uint Id { private set; get; }

		public BaseGameItem(uint id, Vector2Int position)
		{
			Id = id;
			_position = position;
		}

		public Vector2Int Position
		{
			get { return _position; }
			set { _position = value; }
		}

		public int x
		{
			get { return _position.x; }
			set { _position.x = value; }
		}

		public int y
		{
			get { return _position.y; }
			set { _position.y = value; }
		}

		public Vector2Int Size
		{
			get { return _size; }
		}

		public ItemType Type
		{
			set { _type = value; }
			get { return _type; }
		}

		public bool IsOneSize
		{
			get { return true; }
		}
	}
	
	public class ColoredGameItem : BaseGameItem
	{
		public Color Color { private set; get; }

		public ColoredGameItem(uint id, Vector2Int position, Color color, ItemType type = ItemType.FreePart) : base(id, position)
		{
			_type = type;
			Color = color;
		}
	}
}
