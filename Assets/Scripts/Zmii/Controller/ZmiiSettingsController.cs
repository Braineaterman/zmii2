﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace org.yastudio.Zmii
{
	public class ZmiiSettingsController
	{
		private ZmiiSettingsModel _settings;
		private ZmiiProgressSettings _progressSettings;

		private int _collectedItems = 0;
		private int _speedDecreaseCounter = 0;

		public  ZmiiSettingsController()
		{
			// TODO Read data from config
			_settings = new ZmiiSettingsModel();
			_settings.initial = new ZmiiInitialSettings()
			{
				blocks_count = 45,
				speed = 500,
				snake_length = 25
			};

			_progressSettings = new ZmiiProgressSettings()
			{
				bonus_multiplier = 1,
				speed_decrease_quantity = 2,
				speed_decrease_value = 5
			};
		}

		public bool AppendItem()
		{
			_collectedItems++;
			_speedDecreaseCounter++;
			// TODO then read new config if needed

			if (_speedDecreaseCounter < _progressSettings.speed_decrease_quantity) return false;
			_speedDecreaseCounter = 0;
			return true;
		}

		public ZmiiInitialSettings InitialSettings
		{
			get { return _settings.initial; }
		}

		public ZmiiProgressSettings ProgressSettings
		{
			get { return _progressSettings; }
		}
	}
}
