﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Yastudio.Zmii.Utils;
using Yastudio.Zmii.Utils.Colors;
using Debug = UnityEngine.Debug;

namespace org.yastudio.Zmii
{
	public enum Direction { None = -1, North = 0, East = 1, South = 2, West = 3 }
	
	public class ZmiiGameController : MonoBehaviour, IZmiiControlHandler
	{
		private const int MIN_DELAY = 10;

		private System.Random _rnd;
		private int _delay = 150;
		private float _delaySeconds = 0.15F;
		private IColorManager _colors;

		private ZmiiSettingsController _settingsController;
		
		private Direction _direction = Direction.North;
		private Direction _temporaryDirection = Direction.None;// Will be applied at the next move

		private List<ColoredGameItem> _snake;
		private Stopwatch _timer;
		private ZmiiMap _map;

		public event Action<BaseGameItem> OnItemCreated;
		public event Action<BaseGameItem> OnItemChanged;
		public event Action<BaseGameItem> OnItemRemoved;
        public event Action OnBeforeTick;
        public event Action<BaseGameItem> OnAfterTick;

        private void Awake()
		{
			Delay = 100;
			_rnd = new System.Random();
			_map = new ZmiiMap();
		}

		public void StartGame()
		{
			_colors = new BasicColorManager();
			_settingsController = new ZmiiSettingsController();
			_timer = Stopwatch.StartNew();
			InitSnake(10);
			
			for (var i = 0; i < 20; i++)
				CreateFreePartRandom();
			
			for (var i = 0; i < 10; i++)
				CreateWallRandom();

			for (var i = 0; i <= 100; i++)
			{
				CreateWall(new Vector2Int(-50 + i, -50));
				CreateWall(new Vector2Int(-50 + i, 50));
			}

			for (var i = 0; i <= 98; i++)
			{
				CreateWall(new Vector2Int(-50, -49 + i));
				CreateWall(new Vector2Int(50, -49 + i));
			}
			
			InvokeRepeating(nameof(Tick), 1.0f, _delaySeconds);
		}

		private void InitSnake(int size)
		{
			_snake = new List<ColoredGameItem>();
			for (var i = 0; i <= size; i++)
			{
				var ce = new ColoredGameItem(_map.NextId, new Vector2Int(i, 0),  _colors.RandomColor, ItemType.Snake);
				_snake.Add(ce);
				_map.Add(ce);
				OnItemCreated.Dispatch(ce);
			}
		}

		private void CreateFreePartRandom()
		{
			CreateFreePart(GetFreeRandomCoordinates());
		}

		private void CreateWallRandom()
		{
			CreateWall(GetFreeRandomCoordinates());
		}

		private void CreateFreePart(Vector2Int position)
		{
			CreateItem(_colors.RandomColor, position);
		}

		private void CreateWall(Vector2Int position)
		{
			CreateItem(_colors.WallColor, position, ItemType.Wall);
		}

		private void CreateItem(Color color, Vector2Int position, ItemType type = ItemType.FreePart)
		{	
			var freePart = new ColoredGameItem(_map.NextId, new Vector2Int(position.x, position.y),  color, type);
			_map.Add(freePart);
			OnItemCreated.Dispatch(freePart);
		}

		private Vector2Int GetFreeRandomCoordinates()
		{
			Vector2Int pos = Vector2Int.zero;
			var freeCellFound = false;
			while (!freeCellFound)
			{
				pos = new Vector2Int(_rnd.Next(-50, 50), _rnd.Next(-50, 50));
				freeCellFound = _map.IsCellFree(pos);
			}

			return pos;
		}

		private void Tick()
		{
            OnBeforeTick.Dispatch();
			//Debug.Log(_timer.ElapsedMilliseconds);
			_timer.Reset();
			_timer.Start();
			MoveSnake();
			OnAfterTick.Dispatch(SnakeHead);
		}

		private void ChangeSpeed(float speed)
		{
			
		}
		
		protected int Delay
		{
			set
			{
				_delay = value < MIN_DELAY ? MIN_DELAY : value;
				_delaySeconds = (float) _delay / 1000;
			}
			get { return _delay; }
		}

		#region Zmii

		private void MoveSnake()
		{
			if (_temporaryDirection != Direction.None)
			{	
				Debug.Log("APPLIED: " + Time.realtimeSinceStartup);
				_direction = _temporaryDirection;
				_temporaryDirection = Direction.None;
			}
			
			var next = NextPosition(SnakeHead);
			
			var collideElem = _map.GetAt(next);
			if (collideElem != null) {
				var gameOver = HandleCollision(collideElem);

				if (gameOver)
				{
					CancelInvoke(nameof(Tick));
					return;
				}
			
				// Recalculate next position for new element
				//next = NextPosition(SnakeHead);
				//MoveSnake();
				return;
			}

			for (int i = _snake.Count - 1; i >= 0; i--)
			{
				var tx = _snake[i].x;
				var ty = _snake[i].y;
				_map.Move(_snake[i], next.x, next.y);
				next.x = tx;
				next.y = ty;
				OnItemChanged.Dispatch(_snake[i]);
			}
		}

		private BaseGameItem SnakeHead => _snake[_snake.Count - 1];

		private Vector2Int NextPosition(BaseGameItem item)
		{
			switch (_direction)
			{
				case Direction.North:
					return new Vector2Int(item.Position.x + 1, item.Position.y);
				case Direction.East:
					return new Vector2Int(item.Position.x, item.Position.y - 1);
				case Direction.South:
					return new Vector2Int(item.Position.x - 1, item.Position.y);
				case Direction.West:
					return new Vector2Int(item.Position.x, item.Position.y + 1);
			}

			return item.Position;
		}

		private bool HandleCollision(BaseGameItem collidedElement)
		{
			if (collidedElement.Type == ItemType.Snake)
				return true;
			
			if (collidedElement.Type == ItemType.Wall)
				return false;

            if (collidedElement.Type == ItemType.FreePart)
                FreePartCollision(collidedElement as ColoredGameItem);
			return false;
		}

        private void FreePartCollision(ColoredGameItem part)
        {
            var sameColorCount = 1;

            for (int i = _snake.Count - 1; i >= 0; i--)
            {
                if (part.Color == _snake[i].Color)
                    sameColorCount++;
                else
                    break;
            }

            if (sameColorCount >= 3 && (_snake.Count - sameColorCount) >= 2)
            {
	            var newDirection = GetDirectionBetween(_snake[_snake.Count - sameColorCount],
		            _snake[_snake.Count - sameColorCount + 1]);

	            if (newDirection != Direction.None)
		            _direction = newDirection;
	            
                for(var i = 1; i < sameColorCount; i++)
                {
                    var item = SnakeHead;
                    _map.Remove(_snake[_snake.Count - 1]);
                    OnItemRemoved.Dispatch(item);
                    _snake.RemoveAt(_snake.Count - 1);
                }
                _map.Remove(part);
                OnItemRemoved.Dispatch(part);
            } else {
                _snake.Add(part);
                part.Type = ItemType.Snake;
                CreateFreePartRandom();
                CreateFreePartRandom();
			    CreateWallRandom();
			    CreateWallRandom();
            }
        }

		#endregion

		#region IZmiiControlHandler implementation

		public void SetDirection(Direction direction)
		{
			if (direction == Direction.None)
				return;
			
			if (
				(_direction == Direction.North && direction != Direction.South) ||
				(_direction == Direction.South && direction != Direction.North) ||
				(_direction == Direction.West && direction != Direction.East) ||
				(_direction == Direction.East && direction != Direction.West)
			)
			{
				_temporaryDirection = direction;
				//_isDirectionChanged = true;
			}
		}

		public Direction CurrentDirection
		{
			get { return _direction; }
		}

		#endregion

		#region Helpers

		protected Direction GetDirectionBetween(BaseGameItem from, BaseGameItem to)
		{
			if (to.x > from.x)
				return Direction.North;
			
			if (to.x < from.x)
				return Direction.South;
			
			if (to.y > from.y)
				return Direction.West;
			
			if (to.y < from.y)
				return Direction.East;

			return Direction.None;
		}

		#endregion

		public static ZmiiGameController Create(GameObject parent)
		{
			var instance = parent.AddComponent<ZmiiGameController>();
			return instance;
		}
	}

	public class ZmiiMap
	{
		private uint _nextId = 0;
		private Dictionary<long, BaseGameItem> _map;
		private Dictionary<uint, BaseGameItem> _items;
		
		public ZmiiMap()
		{
			_map = new Dictionary<long, BaseGameItem>();
			_items = new Dictionary<uint, BaseGameItem>();
		}

		public void Add(BaseGameItem item)
		{
			if (item.IsOneSize)
			{
				_map[KeyFromPosition(item.Position)] = item;
			}
			else
			{
				for (var i = 0; i < item.Size.x; i++)
				{
					for (var j = 0; j < item.Size.x; j++)
						Add(item.Position.x + i, item.Position.y + j, item);
				}
			}
			_items[item.Id] = item;
		}

		private void Add(int x, int y, BaseGameItem item)
		{
			_map[KeyFromPosition(x, y)] = item;
			_items[item.Id] = item;
		}

		public void Move(BaseGameItem item, int x, int y)
		{
			Remove(item);
			
			item.x = x;
			item.y = y;
			Add(item);
		}

		public void Remove(BaseGameItem item)
		{
			if (item.IsOneSize)
				_map.Remove(KeyFromPosition(item.Position));
			else
			{
				for (var i = 0; i < item.Size.x; i++)
				{
					for (var j = 0; j < item.Size.x; j++)
						Remove(item.Position.x + i, item.Position.y + j);
				}
			}
			_items.Remove(item.Id);
		}

		private void Remove(int x, int y)
		{
			_map.Remove(KeyFromPosition(x, y));
		}

		private long KeyFromPosition(Vector2Int pos)
		{
			return KeyFromPosition(pos.x, pos.y);
		}

		private long KeyFromPosition(int x, int y)
		{
			return (((long) x - int.MinValue) << 32) + ((long) y - int.MinValue);
		}

		public BaseGameItem GetAt(Vector2Int pos)
		{
			var key = KeyFromPosition(pos);
			return _map.ContainsKey(key) ? _map[key] : null;
		}

		public BaseGameItem GetAt(int x, int y)
		{
			return GetAt(new Vector2Int(x, y));
		}

		public bool IsCellFree(Vector2Int pos)
		{
			return !_map.ContainsKey(KeyFromPosition(pos));
		}
		
		public uint NextId
		{
			get { return _nextId++; }
		}

		public Dictionary<uint, BaseGameItem> Items
		{
			get { return _items; }
		}

		public int Count
		{
			get { return _map.Count; }
		}
	}

	public interface IZmiiControlHandler
	{
        event Action OnBeforeTick;
		void SetDirection(Direction direction);
		Direction CurrentDirection { get; }
	}
}
