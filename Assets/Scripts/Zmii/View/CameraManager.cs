using UnityEngine;

namespace org.yastudio.Zmii
{
    public class CameraManager
    {
        private readonly Camera _camera;
        private readonly Vector3 _diff;
        private readonly float _verDiff;

        private float _xMin;
        private float _xMax;
        private float _yMin;
        private float _yMax;
        
        public CameraManager(Camera camera)
        {
            _camera = camera;
            _diff = (_camera.WorldToScreenPoint(Vector3.zero) - _camera.WorldToScreenPoint(new Vector3(1, 0, 0))) * -2;
            CalculateBounds();
        }

        public void MoveHorizontally(int pixels)
        {
            var diff = (float) pixels / _diff.x;
            //Debug.Log(diff);
            var position = _camera.transform.position;
            position = new Vector3(position.x + diff, position.y/* + diff */, position.z - diff);
            // ReSharper disable once Unity.InefficientPropertyAccess
            _camera.transform.position = position;
        }

        public void MoveVertically(int pixels)
        {
            var diff = (float)pixels / _diff.y;
            //Debug.Log(diff);
            var position = _camera.transform.position;
            position = new Vector3(position.x + diff, position.y, position.z + diff);
            // ReSharper disable once Unity.InefficientPropertyAccess
            _camera.transform.position = position;
        }

        protected void CalculateBounds()
        {
            var bound = Mathf.Min(Screen.width * 0.1F, Screen.height * 0.1F);
            _xMin = bound;
            _xMax = Screen.width - _xMin;
            _yMin = bound;
            _yMax = Screen.height - _yMin;
        }

        public void CheckBounds(Vector3 position)
        {
            var pos = _camera.WorldToScreenPoint(position);
            if (pos.x < _xMin)
            {
                //Debug.Log("Left!");
                MoveHorizontally(-200);
            }
            if (pos.x > _xMax)
            {
                //Debug.Log("Right!");
                MoveHorizontally(200);
            }
            if (pos.y < _yMin)
            {
                //Debug.Log("Down!");
                MoveVertically(Mathf.FloorToInt(pos.y - _yMin));
            }
            if (pos.y > _yMax)
            {
                //Debug.Log("Up!");
                //MoveVertically(200);
                MoveVertically(Mathf.FloorToInt(pos.y - _yMax));
            }
        }
    }
}