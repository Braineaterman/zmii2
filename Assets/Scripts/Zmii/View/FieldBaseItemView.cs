using UnityEngine;

namespace org.yastudio.Zmii
{
    public class FieldBaseItemView : MonoBehaviour
    {
        private MovableItemBehaviour _movableComponent;

        public void Move(float x, float y, float z)
        {
            if (!IsMovable)
                return;

            if (_movableComponent == null)
                _movableComponent = gameObject.GetOrCreateComponent<MovableItemBehaviour>();
            
            _movableComponent.Move(x, y, z);
        }

        protected virtual bool IsMovable => true;
    }
}