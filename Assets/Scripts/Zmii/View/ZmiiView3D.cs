﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Object = UnityEngine.Object;

namespace org.yastudio.Zmii
{

	public class ZmiiView3D : MonoBehaviour
	{
		public const float CELL_SIZE = 0.12F;

		private CameraManager _cameraManager;
		private GameObject _viewWrapper;
		private Dictionary<uint, FieldBaseItemView> _items;
		private ZmiiGameController _controller;

		private FieldBaseItemView _head;

		private static readonly int Color = Shader.PropertyToID("_Color");
		private static readonly int DestroyTrigger = Animator.StringToHash(@"destroy");

		public static ZmiiView3D Create(ZmiiGameController controller, GameObject view)
		{
			var result = view.AddComponent<ZmiiView3D>();
			result.Init(controller, view);
			return result;
		}

		protected void Init (ZmiiGameController controller, GameObject view)
		{
			_cameraManager = new CameraManager(Camera.main);
			_controller = controller;
			_viewWrapper = view;
			_items = new Dictionary<uint, FieldBaseItemView>();

			_controller.OnItemCreated += OnItemCreated;
			_controller.OnItemChanged += OnItemChanged;
            _controller.OnItemRemoved += OnItemRemoved;
            _controller.OnAfterTick += OnAfterTick;
        }

		private void Update()
		{
			if (_head != null)
				_cameraManager.CheckBounds(_head.transform.position);
		}

		private void OnAfterTick(BaseGameItem head)
		{
			if (_items.ContainsKey(head.Id))
			{
				_head = _items[head.Id];
				//var headPosition = headItem.transform.position;
				//_cameraManager.CheckBounds(headPosition);
				//Debug.Log(Camera.main.WorldToScreenPoint(headPosition));
				//Camera.main.transform.position = new Vector3( -10 + headPosition.x / 2, 14, -10 + headPosition.x / 2);
				//_cameraManager.HorizontalPosition += 0.1F;
				//_cameraManager.MoveVertically(100);
				//_cameraManager.MoveHorizontally(100);
			}
		}

		private void OnItemCreated(BaseGameItem item)
		{
			switch (item.Type)
			{
				case ItemType.FreePart:
				case ItemType.Wall:
				case ItemType.Snake:
					CreateColoredElement(item as ColoredGameItem);
					break;
			}
		}

		private void OnItemChanged(BaseGameItem item)
		{	
			if (_items.ContainsKey(item.Id))
				_items[item.Id].Move(CELL_SIZE * (float)item.Position.x, 0, CELL_SIZE * (float)item.Position.y);
			
			//if (_items.ContainsKey(item.Id))
			//	_items[item.Id].transform.position = new Vector3(CELL_SIZE * (float)item.Position.x, 0, CELL_SIZE * (float)item.Position.y);
        }

        private void OnItemRemoved(BaseGameItem item)
        {
            if (_items.ContainsKey(item.Id))
            {
	            Animator anim = _items[item.Id].gameObject.GetComponent<Animator> ();
	            anim.SetTrigger (DestroyTrigger);
                //Object.Destroy(_items[item.Id].gameObject);
                _items.Remove(item.Id);
            }
        }

        private void CreateColoredElement(ColoredGameItem item)
		{
			if (item == null)
				return; // TODO Throw exception here?
			
			var coloredCube = _viewWrapper.InstantiatePrefab(@"Cube", new Vector3(CELL_SIZE * (float)item.Position.x, 0, CELL_SIZE * (float)item.Position.y));
		
			//var rend = coloredCube.transform.GetChild(0).gameObject.GetComponent<Renderer>();
			//rend.material.SetColor("_Color", item.Color);
			
			MaterialPropertyBlock properties = new MaterialPropertyBlock();
			properties.SetColor(
				Color, item.Color
			);
			coloredCube.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().SetPropertyBlock(properties);

			_items[item.Id] = coloredCube.GetOrCreateComponent<FieldBaseItemView>();
		}
	}

}
