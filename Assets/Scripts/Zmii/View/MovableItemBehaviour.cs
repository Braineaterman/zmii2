using System;
using UnityEditor;
using UnityEngine;

namespace org.yastudio.Zmii
{
    public class MovableItemBehaviour : MonoBehaviour
    {
        private const float SPEED = 0.1F;
        
        private Vector3 _anchor;
        private Vector3 _diff;
        private float _magnitude = 1;

        private void Update()
        {
            if (IsMoving)
            {
                _magnitude += Time.deltaTime / SPEED;
                transform.position = _anchor + (_diff * _magnitude);
            }
        }

        public void Move(float x, float y, float z)
        {
            if (IsMoving)
                transform.position = _anchor + _diff;

            _anchor = transform.position;
            _diff = new Vector3(x - _anchor.x, y - _anchor.y, z - _anchor.z);
            _magnitude = 0;
        }

        public bool IsMoving => _magnitude < 1;
    }
}