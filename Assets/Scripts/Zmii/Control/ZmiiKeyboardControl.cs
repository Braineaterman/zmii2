﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace org.yastudio.Zmii
{
	public class ZmiiKeyboardControl : MonoBehaviour {
		
		public IZmiiControlHandler Target { set; get; }
		void Update ()
		{
			if (Input.GetKey(KeyCode.UpArrow))
				Target.SetDirection(Direction.North);
			
			if (Input.GetKey(KeyCode.DownArrow))
				Target.SetDirection(Direction.South);
			
			if (Input.GetKey(KeyCode.RightArrow))
				Target.SetDirection(Direction.East);
			
			if (Input.GetKey(KeyCode.LeftArrow))
				Target.SetDirection(Direction.West);
		}

		public static ZmiiKeyboardControl Create(GameObject parent, IZmiiControlHandler target)
		{
			var instance = parent.AddComponent<ZmiiKeyboardControl>();
			instance.Target = target;
			return instance;
		}
	}
}

