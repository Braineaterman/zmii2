﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace org.yastudio.Zmii
{
	public class ZmiiKeyboard2Control : MonoBehaviour {
        private bool _leftClickFired;
        private bool _rightClickFired;

        private IZmiiControlHandler _target;
		public IZmiiControlHandler Target {
            set {
                _target = value;
                //_target.OnBeforeTick += CheckInput;
            }
            get
            {
                return _target;
            }
        }

		private void OnGUI()
		{
			if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.RightArrow)
				RotateDirection(true);
			
			if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.LeftArrow)
				RotateDirection(false);
		}

		void Update ()
        {
	        _rightClickFired = _leftClickFired = false;
	        //Debug.Log(Time.deltaTime);
	        //CheckInput();
        }

        private void CheckInput()
        {
	        if (Input.GetKeyDown(KeyCode.RightArrow) && !_rightClickFired)
	        {
		        _rightClickFired = true;
		        RotateDirection(true);
	        }

	        if (Input.GetKeyDown(KeyCode.LeftArrow) && !_leftClickFired)
	        {
		        _leftClickFired = true;
		        RotateDirection(false);
	        }
        }

		private void RotateDirection(bool clockwise)
		{
			var dir = 0;
			Debug.Log("FIRED: " + Time.realtimeSinceStartup);

			if (clockwise)
			{
				dir = (int) Target.CurrentDirection + 1;
				if (dir > 3)
					dir = 0;
			}
			else
			{
				dir = (int) Target.CurrentDirection - 1;
				if (dir < 0)
					dir = 3;
			}
			Target.SetDirection((Direction) dir);
		}

		public static ZmiiKeyboard2Control Create(GameObject parent, IZmiiControlHandler target)
		{
			var instance = parent.AddComponent<ZmiiKeyboard2Control>();
			instance.Target = target;
			return instance;
		}
	}
}

