﻿using UnityEngine;
using System.Collections.Generic;

namespace Yastudio.Zmii.Utils.Colors
{
	public class BasicColorManager : IColorManager
	{
		private static List<Color> Colors;
		private static System.Random Rand;

		public BasicColorManager()
		{
			Rand = new System.Random();

			Colors = new List<Color>();
			Colors.Add(new Color32(244, 67, 54, 255));//#f44336 244 67 54
			//Colors.Add(new Color32(233, 30, 99, 255));//#e91e63 233 30 99
			//Colors.Add(new Color32(156, 39, 176, 255));//#9c27b0 156 39 176
			Colors.Add(new Color32(103, 58, 183, 255));//#673ab7 103 58 183
			//Colors.Add(new Color32(63, 81, 181, 255));//#3f51b5 63 81 181
			//Colors.Add(new Color32(33, 150, 243, 255));//#2196f3 33 150 243
			Colors.Add(new Color32(3, 169, 244, 255));//#3a9f4 3 169 244
			//Colors.Add(new Color32(0, 188, 212, 255));//#0bcd4 0 188 212
			//Colors.Add(new Color32(0, 150, 136, 255));//#09688 0 150 136
			Colors.Add(new Color32(76, 175, 80, 255));//#4caf50 76 175 80
			//Colors.Add(new Color32(139, 195, 74, 255));//#8bc34a 139 195 74
			//Colors.Add(new Color32(205, 220, 57, 255));//#cddc39 205 220 57
			Colors.Add(new Color32(255, 235, 59, 255));//#ffeb3b 255 235 59
			//Colors.Add(new Color32(255, 193, 7, 255));//#ffc17 255 193 7
			//Colors.Add(new Color32(255, 152, 0, 255));//#ff980 255 152 0
			Colors.Add(new Color32(255, 87, 34, 255));//#ff5722 255 87 34
		}

		public Color RandomColor => (Colors[Rand.Next(Colors.Count - 1)]);

		public Color WallColor => Color.grey;
	}
}
