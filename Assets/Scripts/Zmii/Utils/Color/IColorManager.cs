﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yastudio.Zmii.Utils.Colors
{
    public interface IColorManager
    {
        Color RandomColor { get; }

        Color WallColor { get; }
    }
}
