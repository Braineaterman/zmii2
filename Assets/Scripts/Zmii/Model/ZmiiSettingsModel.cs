﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace org.yastudio.Zmii
{
	public struct ZmiiSettingsModel
	{
		public ZmiiInitialSettings initial;
	}

	public struct ZmiiInitialSettings
	{
		// ReSharper disable once InconsistentNaming
		public int snake_length;
		public int speed;
		public int blocks_count;
		public int max_speed;
	}

	public struct ZmiiProgressSettings
	{
		public int speed_decrease_quantity;
		public int speed_decrease_value;
		public int bonus_multiplier;
	}
}
