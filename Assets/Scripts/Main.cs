﻿using org.yastudio.Zmii;
using UnityEngine;

public class Main : MonoBehaviour
{
	private ZmiiGameController _game;
	private ZmiiView3D _view;
	private ZmiiKeyboard2Control _control;

	[SerializeField] private GameObject _gameWrapper;
	
	void Awake () {
		_game = ZmiiGameController.Create(gameObject);
		_view = ZmiiView3D.Create(_game, _gameWrapper);
		_control = ZmiiKeyboard2Control.Create(gameObject, _game);
		_game.StartGame();
	}
}
